using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace MongoDBMVCApp.Models
{
    public class Computer
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = default!;
        [Display(Name = "Model name")]
        public string Name { get; set; } = default!;
        [Display(Name = "Year of issue")]
        public int Year { get; set; }
        public string ImageId { get; set; } = default!;
        public bool HasImage()
        {
            return !String.IsNullOrWhiteSpace(ImageId);
        }
    }

    public class ComputerFilter
    {
        public string ComputerName { get; set; } = default!;
        public int? Year { get; set; }
    }

    public class ComputerList
    {
        public IEnumerable<Computer> Computers { get; set; } = default!;
        public ComputerFilter Filter { get; set; } = default!;
    }
}