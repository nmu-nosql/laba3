using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDBMVCApp.Models;

namespace MongoDBMVCApp.Service
{
    public class ComputerService
    {
        private IMongoDatabase db;
        private IGridFSBucket gridFS;
        public ComputerService()
        {
            MongoClient client = new MongoClient(GetConnectionString());
            db = client.GetDatabase("laba3");
            gridFS = new GridFSBucket(db);
        }

        public IMongoCollection<Computer> Computers
        {
            get { return db.GetCollection<Computer>("Computers"); }
        }

        public async Task<IEnumerable<Computer>> Find(int? year, string name)
        {
            var builder = new FilterDefinitionBuilder<Computer>();
            var filter = builder.Empty;

            if(!String.IsNullOrWhiteSpace(name))
            {
                filter = filter & builder.Regex("Name", new MongoDB.Bson.BsonRegularExpression(name));
            }

            if(year.HasValue)
            {
                filter = filter & builder.Eq("Year", year.Value);
            }

            return await Computers.Find(filter).ToListAsync();
        }

        public async Task<Computer> Get(string id)
        {
            return await Computers.Find(
                new BsonDocument(
                    "_id", new ObjectId(id)
                )
            ).FirstOrDefaultAsync();
        }

        public async Task Create(Computer computer)
        {
            await Computers.InsertOneAsync(computer);
        }

        public async Task Update(Computer computer)
        {
            await Computers.ReplaceOneAsync(
                new BsonDocument(
                    "_id", new ObjectId(computer.Id)
                ),
                computer
            );
        }

        public async Task Delete(string id)
        {
            await Computers.DeleteOneAsync(
                new BsonDocument(
                    "_id", new ObjectId(id)
                )
            );
        }

        public async Task<byte[]> GetImage(string id)
        {
            return await gridFS.DownloadAsBytesAsync(new ObjectId(id));
        }

        public async Task SaveImage(string id, Stream imageStream, string imageName)
        {
            Computer computer = await Get(id);
            if(computer.HasImage())
            {
                await gridFS.DeleteAsync(new ObjectId(computer.ImageId));
            }
            ObjectId imageId = await gridFS.UploadFromStreamAsync(imageName, imageStream);
            computer.ImageId = imageId.ToString();
            var filter = Builders<Computer>.Filter.Eq("_id", new ObjectId(computer.Id));
            var update = Builders<Computer>.Update.Set("ImageId", computer.ImageId);
            await Computers.UpdateOneAsync(filter, update);
        }

        private string GetConnectionString()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot config = builder.Build();
            return config.GetConnectionString("mongodb") ?? throw new InvalidOperationException("connection can not be null");
        }
    }
}