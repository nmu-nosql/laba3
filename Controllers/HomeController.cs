﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using mvc_project.Models;

using MongoDBMVCApp.Service;
using MongoDBMVCApp.Models;
using System;

namespace mvc_project.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ComputerService service = new ComputerService();

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public async Task<IActionResult> Index(ComputerFilter filter)
    {
        var computers = await service.Find(filter.Year, filter.ComputerName);
        var model = new ComputerList { Computers = computers, Filter = filter };
        return View(model);
    }

    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(Computer computer)
    {
        await service.Create(computer);
        return RedirectToAction("Index");
    }

    public async Task<IActionResult> Edit(string id)
    {
        Computer computer = await service.Get(id);
        if(computer == null)
        {
            return NotFound("No computer with such id");
        }
        return View(computer);
    }

    [HttpPost]
    public async Task<IActionResult> Edit(Computer computer)
    {
        await service.Update(computer);
        return RedirectToAction("Index");
    }

    public async Task<IActionResult> Delete(string id)
    {
        await service.Delete(id);
        return RedirectToAction("Index");
    }

    [HttpPost]
    public async Task<IActionResult> AttachImage(string id, IFormFile uploadedFile)
    {

        if(uploadedFile != null) 
        {
            var tmp = Directory.GetCurrentDirectory() + '/' + uploadedFile.FileName;
            //create file in local filesystem
            using (Stream fs = new FileStream(tmp, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fs);
            }

            //upload it to gridfs
            using(Stream fs = new FileStream(tmp, FileMode.Open))
            {
                await service.SaveImage(id, fs, uploadedFile.FileName);
            }

            //remove from local filesystem
            System.IO.File.Delete(tmp);
        }
        return RedirectToAction("Index");
    }

    public async Task<IActionResult> AttachImage(string id)
    {
        Computer computer = await service.Get(id);
        if(computer == null) return NotFound("No computer with such id");
        return View(computer);
    }

    public async Task<IActionResult> GetImage(string id)
    {
        var image = await service.GetImage(id);
        if(image == null) return NotFound("No image with such id");
        return File(image, "image/jpg");
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
